import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// Java tests for family members, activities, and age and if they are still at home
// Tests are performed using the assert methods
// I recognize Dainon, Walter, and Marvel from my group in helping with the assert method code understanding
// Various YouTube tutorial videos including:
// Dr. Brian Fraser - https://www.youtube.com/watch?v=GaNW6Q__5jc&list=PL-suslzEBiMobIYkpy0ijFzSZF1w_ok8r&ab_channel=BrianFraser
// Dr. Brian Fraser - https://www.youtube.com/watch?v=Bld3644bIAo&ab_channel=BrianFraser
// Jakob Jenkov - http://tutorials.jenkov.com/java-unit-testing/asserts.html
// https://www3.ntu.edu.sg/home/ehchua/programming/java/JavaUnitTesting.html
// https://www.lambdatest.com/blog/junit-assertions-example-for-selenium-testing/
// https://www.journaldev.com/21681/junit-assertions

class FamilyMembersTest {
    FamilyMembers sylvia = new FamilyMembers("Sylvia", "Dance", 19);
    FamilyMembers alex = new FamilyMembers("Alex","Baseball",16);
    FamilyMembers nullFamilyMember;

    // assertEquals Test - set Sylvia and expected Sylvia
    @Test
    void getName() {
        try {
            assertEquals("Sylvia", sylvia.getName());
        } catch (org.opentest4j.AssertionFailedError a) {
            System.err.println(a.toString());
        }
    }

    // assertNotEquals Test - set Alex and is expected not to be Sylvia
    @Test
    void get2ndName() {
        try {
            assertNotEquals("Alex", sylvia.getName());
        } catch (org.opentest4j.AssertionFailedError a) {
            System.err.println(a.toString());
        }
    }
    // assertEquals Test - another test but on the activity
    @Test
    void getActivity() {

        try{assertEquals("Dance", sylvia.getActivity());
        }
        catch (org.opentest4j.AssertionFailedError a){
            System.err.println(a.toString());
        }
    }

    // assertTrue Test - default boolean setAtHome method is true
    @Test
    void isAtHome() {
        try{assertTrue(sylvia.isAtHome());
        }
        catch (org.opentest4j.AssertionFailedError a){
            System.err.println(a.toString());
        }
    }

    // assertFalse Test - changed the boolean setAtHome method to false
    @Test
    void isNotAtHome() {
        sylvia.setAtHome(false);
        try{assertFalse(sylvia.isAtHome());
        }
        catch (org.opentest4j.AssertionFailedError a){
            System.err.println(a.toString());
        }
    }

    // assertNull Test - set a nullFamilyMember without a value at the beginning
    @Test
    void isNullFamilyMember(){
        try{assertNull(nullFamilyMember);
        }
        catch (org.opentest4j.AssertionFailedError a){
            System.err.println(a.toString());
        }
    }

    // assertNotNull Test - value can be set to true or false as long as it has a value
    @Test
    void setAtHome() {
        sylvia.setAtHome(false);
        try{assertNotNull(sylvia.isAtHome());
        }
        catch (org.opentest4j.AssertionFailedError a){
            System.err.println(a.toString());
        }
    }

    // assertSame Test - set the nullFamilyMember to equal sylvia and check if they are the same
    @Test
    void sameFamilyMember() {
        FamilyMembers nullFamilyMember = sylvia;
        try{assertSame(sylvia, nullFamilyMember);
        }
        catch (org.opentest4j.AssertionFailedError a){
            System.err.println(a.toString());
        }
    }

    // assertNotSame Test - comparing sylvia to alex
    @Test
    void notSameFamilyMember() {
        try {assertNotSame(sylvia, alex);
        }
        catch (org.opentest4j.AssertionFailedError a){
            System.err.println(a.toString());
        }
    }

    // assertArrayEquals Test - I set up a familyArray that consist of sylvia and alex, and
    // expecting both members (Alex and Sylvia) to be in the array
    @Test
    void familyArrayTest(){
        String familyArray[] = {sylvia.getName(),alex.getName()};
        String expected[] = {"Sylvia", "Alex"};
        try{assertArrayEquals(expected, familyArray);
        }
        catch (org.opentest4j.AssertionFailedError a){
            System.err.println(a.toString());
        }
    }
}
