import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CupTest {

    @Test
    void getLiquidType() {
        Cup c = new Cup("orange juice", 85.5);
        assertEquals("orange juice", c.getLiquidType());
    }

    @Test
    void getPercentageFull() {
        Cup c = new Cup("orange juice", 85.5);
        assertEquals(85.5, c.getPercentFull(), 0.001);
    }

    @Test
    void setLiquidType() {
        Cup c = new Cup("orange juice", 85.5);
        c.setLiquidType("water");
        assertEquals("water", c.getLiquidType());
    }

    @Test
    void testObjectCreation() {
        Cup cup = new Cup("water",75.0);
        assertEquals("water",cup.getLiquidType());
        assertEquals(75, cup.getPercentFull(), 0.001);
    }

    @Test
    void testObjectCreationWithAssertAll() {
        Cup cup = new Cup("water",75.0);
        assertAll("Correctly builds object",
                () -> assertEquals("water",cup.getLiquidType()),
                () -> assertEquals(75, cup.getPercentFull(), 0.001)
        );
    }

    @Test
    void testIsEmpty () {
        Cup cup = new Cup("water",75.0);
        assertFalse(cup.isEmpty());
        assertTrue(!cup.isEmpty());
    }

    @Test
    void testSetLiquidWithNull() {
        Cup cup = new Cup("water",75.0);
        cup.setLiquidType(null);
        assertNotNull(cup.getLiquidType());
    }

    @Disabled("Disabled test until library team fixes bug 2532")
    @Test
    void testExternalLibrary() {
        // Imagine depending on someone else's code...
        fail();
    }

    @Test
    void testSetBadPercentThrows() {
        Cup cup = new Cup("water", 75.0);
        assertThrows(IllegalArgumentException.class,
                () -> cup.setPercentFull(-1)
        );
    }

}