import org.junit.Ignore;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {
    private Student mark = new Student("Mark");
    private Student dainon = new Student("Dainon");
    private Student marvel = new Student("Marvel");
    private Student waltor = new Student("Waltor");
    private Student dustin = new Student("Dustin");
    private Student steve = new Student("Steve");

    @Test
    public void getName() throws Exception{
        assertEquals("Mark", mark.getName());
        assertEquals("Dainon",dainon.getName());
        assertEquals("Marvel",marvel.getName());
        assertEquals("Waltor",waltor.getName());
        assertEquals("Dustin",dustin.getName());
        assertEquals("Steve",steve.getName());
    }
    @Test
    public void testUnhappyToStart() throws Exception{
        assertFalse(mark.isHappy());
        assertFalse(dainon.isHappy());
        assertFalse(marvel.isHappy());
        assertFalse(waltor.isHappy());
        assertFalse(dustin.isHappy());
        assertFalse(steve.isHappy());
    }
    @Test
    public void testHappyAfterPlay() throws Exception{
        mark.playFootball();
        assertTrue(mark.isHappy());
        dainon.playFootball();
        assertTrue(dainon.isHappy());
        marvel.playFootball();
        assertTrue(marvel.isHappy());
        waltor.playFootball();
        assertTrue(waltor.isHappy());
        dustin.playFootball();
        assertTrue(dustin.isHappy());
        steve.playFootball();
        assertTrue(steve.isHappy());
    }


    @Test
    public void name() throws Exception {
        mark.playFootball();
        String msg = mark.getHappyMessage();
        assertEquals("I'm happy!", msg);
    }


}