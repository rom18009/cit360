public class Student {

    private String name;

    private boolean happy = false;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isHappy() {
        return happy;
    }

    public void playFootball() {
        happy = true;
    }

    public String getHappyMessage(){
        if(!happy) {
            throw new IllegalStateException();
        }
        return "I'm happy!";
    }
}
