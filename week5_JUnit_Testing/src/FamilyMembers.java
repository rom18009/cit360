public class FamilyMembers {

    //Variables
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private String activity;
    private int age;
    private boolean atHome = true;

    public boolean isAtHome() {
        return atHome;
    }

    public void setAtHome(boolean atHome) {
        this.atHome = atHome;
    }

    public FamilyMembers(String name, String activity, int age) {
        this.name = name;
        this.activity = activity;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getActivity() {
        return activity;
    }

    public int getAge() {
        return age;
    }
}
