import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Order implements Runnable {

    // variables for order
    private String customer;
    private String pizza;
    private int size;
    private long time;

    public Order(String customer, String pizza, int size) {
        this.customer = customer;
        this.pizza = pizza;
        this.size = size;
        Random random = new Random();
        // Sets the time int in minutes for cooking
        this.time = random.nextInt((int) TimeUnit.MINUTES.toMillis(1));
    }

    @Override
    public void run() {
        System.out.println("\nPlacing " + customer + "'s " + size + '"' + " " + pizza + " Pizza in the oven.");
        System.out.println("\n" + customer + "'s pizza requires more cooking time...");

        // Exception Handling taken from Walter's code example and from https://www.baeldung.com/java-executor-service-tutorial
        try {
            Thread.sleep(time);
        }
        catch ( InterruptedException e ) {
            e.printStackTrace();
        }
        System.out.println("\n" + customer + "'s " + size + '"' + " " + pizza +" Pizza is done. Enjoy!");
    }
}
