import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Cooking {
    public static void main(String[] args) {
        // Oven Max Pizza Capacity variable
        int ovenMax = 4;
        // Sets the Oven Max Capacity in the ExecutorService and ovenMax Threads
        ExecutorService myService = Executors.newFixedThreadPool(ovenMax);

        // Used the example from Brother Tuckett https://bitbucket.org/tuckettt/basicthreads/src/master/src/com/tuckettt/basicthreads/ExecuteDoingSomething.java
        // List of Family Orders, pizza type, and size
        // Pizza names and sizes are taken from Mod Pizza menu: https://modpizza.com/menu
        Order p1 = new Order("Amber", "Caspian", 6);
        Order p2 = new Order("Mark", "Mad Dog", 11);
        Order p3 = new Order("Alex", "Calexico", 11);
        Order p4 = new Order("Tyson", "Jasper", 11);
        Order p5 = new Order("Ben", "Mad Dog", 11);
        Order p6 = new Order("Kinley", "Maddy", 6);

        // Execute the pizza orders (p1 - p2)
        myService.execute(p1);
        myService.execute(p2);
        myService.execute(p3);
        myService.execute(p4);
        myService.execute(p5);
        myService.execute(p6);

        // Shutdown the myService executor service
        myService.shutdown();
    }

}
