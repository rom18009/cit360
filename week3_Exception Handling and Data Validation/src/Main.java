
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // Marvel helped explain exception handling and data validation.
        // I used https://www.includehelp.com/java-programs/handle-multiple-exceptions.aspx
        //   for help in getting the code to come together
        while (true) {
            try {
                // scanner object class
                Scanner input = new Scanner(System.in);

                // user input for num1
                System.out.print("\nEnter the First Number: ");
                float num1 = input.nextFloat();

                // user input for num2
                System.out.println("\nEnter the Second Number: ");
                float num2 = input.nextFloat();

                    // if num2 is zero, throw ArithmeticException Error
                    if (num2 == 0)
                        throw new ArithmeticException();

                // divide num1 by num2 for answer variable
                float answer = num1 / num2;

                // show results
                System.out.println("\nThe answer to " + num1 + " / " + num2 + " is " + answer);
                break;

            } catch (InputMismatchException e) {
                // show if value is invalid i.e. strings, letters, etc.
                System.out.println("\nLetters were entered, use numbers only. Try again.");

            } catch (ArithmeticException e) {
                // when divided by zero. Ask to try again
                System.out.println("\nError: Can not divide by ZERO. Try again.");

            } finally {
                // line separator
                System.out.println("............................................");
            }

        }
    }
}

