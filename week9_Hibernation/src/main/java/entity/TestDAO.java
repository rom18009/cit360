package entity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class TestDAO {
    EntityManagerFactory factory = null;
    EntityManager entityManager = null;

    private static TestDAO single_instance = null;

    private TestDAO()
    {
        factory = HibernateUtils.getEntityManager();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static TestDAO getInstance() {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }

        return single_instance;
    }

    /** Used to get more than one customer from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<Order> getEntrees() {

        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            String sql = "from Order";
            List<Order> cs = (List<Order>)entityManager.createQuery(sql).getResultList();
            entityManager.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return null;
        } finally {
            entityManager.close();
        }

    }

    public boolean addEntree(Order entree) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(entree);
            entityManager.getTransaction().commit();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    public boolean removeEntree(int id) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            Order entree = entityManager.find(Order.class, id);
            entityManager.remove(entree);
            entityManager.getTransaction().commit();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    public boolean updateEntree(int id, String name) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            Order entree = entityManager.find(Order.class, id);
            bank.setName(name);
            entityManager.merge(entree);
            entityManager.getTransaction().commit();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }
}

