package entity;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        options();
    }

    public static void options() {
        int i = 1;
        TestDAO t = TestDAO.getInstance();

        System.out.println("What would you like to do?");
        System.out.println("1- Add an entree");
        System.out.println("2- List available entrees");
        System.out.println("3- Update an entree name");
        System.out.println("4- Remove an entree");
        System.out.println("0- Exit");

        Scanner in = new Scanner(System.in);
        int num = in.nextInt();

        switch (num){

            case 1:
                addEntree(t);
                break;
            case 2:
                listEntrees(t);
                break;
            case 3:
                updateEntree(t);
                break;
            case 4:
                removeEntree(t);
                break;
            case 0:
                i = 0;
                break;
        }

        if(i == 1){
            options();
        }else{
            System.out.println("Exiting...");
            System.exit(1);
        }
    }

}
