package com.cit360.week4.assignment;

public class TruckRequestOut {
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
