package com.cit360.week4.assignment;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

    // combination of this week's example file
    // Marvel Okafor from my group
    // codewithmosh.com
    // https://devqa.io/convert-java-to-json/
    // https://www.javatpoint.com/convert-java-object-to-json
    // https://www.javacodegeeks.com/2015/10/creating-sample-https-server-for-fun-and-profit.html
    // https://www.educative.io/edpresso/how-to-convert-a-java-object-to-json
    // and a co-worker that uses Java to verify the code and the client to server connection
public class Client {
    public static String executeHttpRequest(String verb, String endpoint, String out) throws Exception {
        if (endpoint == null || endpoint.isEmpty()) {
            throw new IllegalArgumentException("URL endpoint is missing");
        }

        // Create object
        URL url = new URL(endpoint);
        // Create connection
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod(verb);
        connection.setRequestProperty("Content-Type", "application/json");

        // Response for object creation
        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(out.getBytes());
        outputStream.flush();

        int code = connection.getResponseCode();
        if (code != HttpURLConnection.HTTP_OK && code != HttpURLConnection.HTTP_CREATED) {
            // Cannot connect to server
            throw new Exception("Something went wrong! \nHTTP error code : " + code);
        }

        // https://www.programcreek.com/java-api-examples/?api=org.apache.http.HttpResponse
        InputStreamReader inputStreamReader = new InputStreamReader((connection.getInputStream()));
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        return bufferedReader.lines().collect(Collectors.joining());
    }

    public static List<Truck> getTrucksByType(String type) throws Exception {
        // https://dzone.com/articles/java-string-format-examples
        String response = executeHttpRequest("GET", "http://localhost:7501/trucks", String.format("{\"type\": \"%s\"}", type));

        ObjectMapper objectMapper = new ObjectMapper();
        Truck[] trucks = objectMapper.readValue(response, Truck[].class);
        return Arrays.asList(trucks);
    }

    public static List<Truck> getAllTrucks() throws Exception {
        String response = executeHttpRequest("GET", "http://localhost:7501/trucks", "");

        ObjectMapper objectMapper = new ObjectMapper();
        Truck[] trucks = objectMapper.readValue(response, Truck[].class);
        return Arrays.asList(trucks);
    }

    public static void main(String[] args) throws Exception {
        List<Truck> trucks = getAllTrucks();
        for (Truck truck : trucks) {
            System.out.println(truck);
        }

        List<Truck> GMCs = getTrucksByType("GMC");
        for (Truck GMC : GMCs) {
            System.out.println(GMC);
            System.out.println("GMCs are really nice and I would love to own one!");
        }

        List<Truck> FORDs = getTrucksByType("FORD");
        for (Truck FORD : FORDs) {
            System.out.println(FORD);
            System.out.println("FORDs are also nice but just not my style!");
        }
    }
}
