package com.cit360.week4.assignment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TruckRequestHandler implements HttpHandler {
    // Setting up Mapper for the conversion
    private final ObjectMapper objectMapper = new ObjectMapper();

    // creating trucks Arraylist
    private final List<Truck> trucks = new ArrayList<>();
    public TruckRequestHandler() {
        // Create trucks
        Truck Canyon = new Truck("Canyon", "GMC");
        Truck Sierra1500 = new Truck("Sierra1500","GMC");
        Truck SierraHD = new Truck("SierraHD", "GMC");
        Truck F150 = new Truck("F150", "FORD");
        Truck F250 = new Truck("F250", "FORD");
        Truck F350 = new Truck("F350", "FORD");

        // Add in the trucks
        trucks.add(Canyon);
        trucks.add(Sierra1500);
        trucks.add(SierraHD);
        trucks.add(F150);
        trucks.add(F250);
        trucks.add(F350);
    }

    @Override
    public void handle(HttpExchange httpExchange)throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(
                httpExchange.getRequestBody(), StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String jsonOut = bufferedReader.lines().collect(Collectors.joining());

        if (jsonOut.isEmpty()) {
            jsonOut = objectMapper.writeValueAsString(trucks);
        }
        else {
            try {
                // This converts the load out using Jackson and adds in the truck model
                TruckRequestOut requestOut = objectMapper.readValue(jsonOut, TruckRequestOut.class);
                String trucksType = requestOut.getType();

                // Matches the type to the trucks
                List<Truck> trucks = new ArrayList<>();
                for (Truck truck : this.trucks) {
                    if (truck.getType().equals(trucksType)) {
                        trucks.add(truck);
                    }
                }

                jsonOut  = objectMapper.writeValueAsString(trucks);
            }
            catch (IOException e) {
                httpExchange.sendResponseHeaders(400, 0);
                writeHttpResponse(httpExchange.getResponseBody(),"Bad Request");
                return;
            }
        }

        Headers headers = httpExchange.getResponseHeaders();
        headers.add("Content-Tyoe", "application/json; charset+UTF-8");

        // Successful writeHttpResponse
        httpExchange.sendResponseHeaders(200, jsonOut.length());
        writeHttpResponse(httpExchange.getResponseBody(), jsonOut);
    }

    public void writeHttpResponse(OutputStream outputStream, String message)throws IOException {
        outputStream.write(message.getBytes());
        outputStream.close();
    }

}

